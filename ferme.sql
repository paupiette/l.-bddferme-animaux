drop database if exists ferme;

create database ferme;
grant all on ferme.* to 'claire'@'localhost';
flush privileges;

use ferme;

/*création des tables*/

create table batiment (
    id_batiment varchar(20) NOT NULL UNIQUE,
    PRIMARY KEY(id_batiment) #identifiant batiment lettre
);

create table logement (
    id_logement int AUTO_INCREMENT,
    PRIMARY KEY (id_logement), #identifiant logement chiffre
    id_batiment varchar(20) NOT NULL, #identifiant batiment
    numero_logement int NOT NULL,
    nombre_place int,
    #CHECK (nombre_place BETWEEN 1 AND 4),
    UNIQUE (id_batiment, numero_logement),
    FOREIGN KEY (id_batiment) REFERENCES batiment(id_batiment) #il faut que id_batiment de la table logement existe dans id_batiment de la table batiment 
);

create table espece (
    id_espece int NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id_espece),
    nom_espece varchar(20) NOT NULL
);

create table animal (
    id_animal int NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id_animal),
    nom_animal varchar(20) NOT NULL,
    id_espece int NOT NULL,
    id_logement int,
    FOREIGN KEY (id_logement) REFERENCES logement(id_logement),
    FOREIGN KEY (id_espece) REFERENCES espece(id_espece)
);

/*1-2.créer 1 batiment par lettre et 100 logements par batiments*/

DELIMITER //
CREATE PROCEDURE creer_batlog(IN nl INT)
BEGIN

    SET @a = 65;
    

    WHILE ( @a < 91) DO
    insert into batiment values ((SELECT CHAR(@a USING utf8)));
    SET @c = 0;
        WHILE (@c < nl) DO
        insert into logement values (NULL, (SELECT CHAR(@a USING utf8)), @c+1, round(rand()*3)+1);
        SET @c = @c + 1;
        END WHILE;
    SET @a = @a + 1;
    END WHILE;

END//
DELIMITER ;
CALL creer_batlog(100);
#select * from batiment;
#select * from logement;


/* 3. créer 2000 animaux */

    /*création d'espèces*/
    insert into espece (nom_espece) values ('poule');
    insert into espece (nom_espece) values ('loup');
    insert into espece (nom_espece) values ('chat');
    insert into espece (nom_espece) values ('chien');
    insert into espece (nom_espece) values ('mouton');
    insert into espece (nom_espece) values ('rat');
    insert into espece (nom_espece) values ('cochon-dinde');
    insert into espece (nom_espece) values ('lapin');

    #select * from espece;

DELIMITER //
CREATE PROCEDURE creer_animaux(IN na INT)
BEGIN

    SET @b = 0;
    
    WHILE ( @b < na) DO

    insert into animal values (null, @b, round(rand()*8), null);
    
    SET @b = @b + 1;
    END WHILE;

END//
DELIMITER ;
CALL creer_animaux(10000);
#select * from animal;




/*4. loger tous les animaux*/























/*tables suplémentaires - ne fonctionnent pas*/

/*create table chantier (
    id_chantier int NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id_chantier),
    etat float 
);

create table chantier_animal (
    id_chantier varchar(20) NOT NULL,
    id_animal varchar(30) NOT NULL,
    FOREIGN KEY (id_chantier) REFERENCES chantier(id_chantier),
    FOREIGN KEY (id_animal) REFERENCES animal(id_animal)
);*/

/* 1. créer 1 batiment par lettre*/

/*DELIMITER //
CREATE PROCEDURE creer_batiments()
BEGIN

    SET @a = 65;

    WHILE ( @a < 91) DO
    insert into batiment values ((SELECT CHAR(@a USING utf8)));
    SET @a = @a + 1;
    END WHILE;

END//
DELIMITER ;
CALL creer_batiments();
#select * from batiment;*/

/*2bis. créer 100 logements*/

/*DELIMITER //
CREATE PROCEDURE creer_logements(IN bat CHAR(1), IN nl INT)
BEGIN

    SET @c = 0;

    WHILE ( @c < nl) DO
    insert into logement values (NULL, bat, @c+1, round(rand()*3)+1);
    SET @c = @c + 1;
    END WHILE;

END//
DELIMITER ;
CALL creer_logements('A', 1);
select * from logement;*/


/*exemple de select et insert avec mysql*/

#insert into animal (nom_animal, espece, ID_logement) values('Huguette', 'poule', (select ID_logement from logement where lettre='W' AND numero_logement=1));

#select nom_animal, espece from animal where ID_logement in (select ID_logement from logement where lettre='W' AND numero_logement=1); 
#select concat(lettre, numero_logement) from logement where lettre='W' AND numero_logement=1;

#select sum(nombre_place) from ferme.logement;
